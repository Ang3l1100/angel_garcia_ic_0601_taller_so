#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

int main(){
    int process;
    float time, executionTime;
    printf("This program pretends to simulate a scheduler. Developed by: Angel Garcia\nPlease enter the number of process we'll have: ");
    scanf("%i",&process);
    int tasks[process];
    printf("Now enter the process's time: ");
    scanf("%f",&time);
    executionTime = 1.000 / process;
    printf("Execution time:%f\n", executionTime);
    do{
        for(int i=0; i<process; i++){
            printf("Executing process %i, wait.\n", i+1);
            sleep(executionTime*10);
        }
        time=time-executionTime;
        printf("\nRemaining time per process: %f, wait.\n\n", time);
        if(time<=0){process=0;}
    }while (process !=0);
    return 0;
}