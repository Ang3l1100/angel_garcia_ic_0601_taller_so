
#!/bin/bash

SUCCESS=0

echo
echo "-------------------------> Eliminar Grupos <-------------------------"
echo "------------------> By: Angel Maldonado Garcia <---------------------"
echo

eliminarGrupo(){
        echo "Introduce el nombre del Grupo que deseas Eliminar: "
        read group
        sudo groupdel $group
        if[ $? -eq $SUCCESS ]
        then
                echo "---> Grupo eliminado Correcctamente"
        else
                echo "---> **El grupo no existe o no fue posble ser Borrado"
        fi
}

eliminarUsuarioGrupo(){
        echo "Introduce el nombre del usuario a eliminar"
        read name
        echo "introduce el grupo en el que se encuentra la persona"
        read principalGroup
        sudo deluser $name $principalGroup
        if[ $? -eq $SUCCESS ]
        then
                echo "---> Usuario [ $name ] eliminado del grupo [ $principalGroup ]"
        else
                echo "---> **El usuario no se ha podido eliminar."
}

